﻿using System;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            double h, a, b, Result = 0;
            Console.Write("Step:");//0,3
            h = double.Parse(Console.ReadLine());
            Console.Write("interval1:");//0,3
            a = double.Parse(Console.ReadLine());
            Console.Write("interval2:");//3,5
            b = double.Parse(Console.ReadLine());
            for (double x = a; x < b; x += h)
            {
                if (x < 1)
                    Result = Math.Atan(1 / x);
                else if (x >= 1 && x < 3)
                    Result = Math.Tan(x + Math.Log(x, 4));
                else if (x >= 3)
                    Result = 1 / 1 + Math.Log(x);
                Console.WriteLine($"If x={Math.Round(x, 1)}  \t result={Math.Round(Result, 5)}");
            }
        }
    }
}
